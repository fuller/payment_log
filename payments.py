#! /usr/bin/python3

from datetime import date
import yaml

startdate = date.fromisoformat("2022-03-15")
monthly_rate_usd = 1.e3

def elapsed_months(t1: date, t0: date) -> int:
    delta = t1 - t0
    elapsed_days = delta.days
    elapsed_years = elapsed_days // 365
    added_months = (elapsed_days % 365) // 30
    return ((12 * elapsed_years) + added_months)

def main():
    today = date.today()
    total_debt = monthly_rate_usd * elapsed_months(today, startdate)

    past_payments = 0
    future_payments = 0

    with open("payment_data.yaml") as f:
        payment_data = yaml.safe_load(f)

    for payment in payment_data["payments"]:
        if today >= date.fromisoformat(payment["date"]):
            past_payments += payment["amount"]
        else:
            future_payments += payment["amount"]
    
    print(f"amount due: ${total_debt - past_payments:.0f}")


if __name__ == '__main__':
    main()
