#! /usr/bin/python3

class IsraeliBank:

    def __init__(self, bank_data: dict):
        self.__bank_data = bank_data
    
    def __check_currency(self, unit0: str, unit1: str):
        if unit0 != unit1:
            raise ValueError("currency unit mismatch")

    def __rate_min_max_calc(self, data: dict, amount: float, currency: str) -> [float, str]:
        try: 
            self.__check_currency(data["unit"], currency)
            return min(max(data["rate"]*amount, data["min"]), data["max"]), data["unit"]
        except ValueError:
            return float('nan'), f'currency unit mismatch: got {currency}, but need {data["unit"]}'

    def transaction_fee(self) -> float:
        """
        standard fee (ILS) assessed per simple account transaction
        """
        return self.__bank_data["transaction"]["direct"]
    
    def securities_transaction_fee(self, amount: float, currency: str) -> [float, str]:
        """
        fee (ILS) incurred for transactions on the Tel Aviv Stock Exchange
        """
        data = self.__bank_data["securities"]["transaction"]
        return self.__rate_min_max_calc(data, amount, currency)


    def forex_fee(self, amount: float, currency: str) -> [float, str]:
        """
        fee (USD) incurred for converting between ILS and a foreign currency
        """
        data = self.__bank_data["foreign-transfer"]["one-time"]
        return self.__rate_min_max_calc(data, amount, currency)

    def wire_fee(self, amount: float, currency: str) -> [float, str]:
        data = self.__bank_data["foreign-transfer"]["one-time"]
        self.__rate_min_max_calc(data, amount)



if __name__ == "__main__":
    print("example: Leumi")

    import yaml

    with open("bank_data.yaml") as f:
        all_data = yaml.safe_load(f)
    
    leumiBank = IsraeliBank(all_data["Leumi"])
    print(leumiBank.transaction_fee())
    print(leumiBank.securities_transaction_fee(3000., "USD"))
    print(leumiBank.securities_transaction_fee(3000., "ILS"))
    print(leumiBank.forex_fee(1000., "USD"))
    print(leumiBank.forex_fee(3000., "ILS"))

    mercantileBank = IsraeliBank(all_data["Mercantile"])

    print(mercantileBank.transaction_fee())
    print(mercantileBank.securities_transaction_fee(3000., "ILS"))
    print(mercantileBank.forex_fee(1000., "USD"))
    