#! /usr/bin/python3

import yaml
from random import randint

with open("tax_brackets.yaml") as f:
    tax_brackets = yaml.safe_load(f)

income = randint(0,1e6)
print(income)

tax = 0
balance = income
for income_floor, tax_rate in reversed(sorted(tax_brackets["IL"]["income_tax_marginals"].items())):
    taxable = max((balance - income_floor), 0)
    tax += taxable*tax_rate
    balance = balance - taxable

print(tax)